/*
    QCommsy
    Copyright (C) 2020  niansa

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/
#include <sstream>

#include <curlpp/cURLpp.hpp>
#include <curlpp/Easy.hpp>
#include <curlpp/Infos.hpp>
#include <curlpp/Options.hpp>

long curlreq(std::stringstream &responsebuffer, std::string SID, std::string URL) {
    std::cout << "Connection details begin" << std::endl;
    std::cout << "URL: " << URL << std::endl;
    std::cout << "SID: " << SID << std::endl;
    std::cout << "Connection details end" << std::endl;
    // Initialise variables
    curlpp::Cleanup cleaner;
    curlpp::Easy request;
    // Set the writer callback to enable cURL to write result in a memory area
    request.setOpt(new curlpp::options::WriteStream(&responsebuffer));
    // Setting the URL to retrive.
    request.setOpt(new curlpp::options::Url(URL));
    // Set SID cookie
    std::list<std::string> header;
    header.push_back("Cookie: SID=" + SID);
    request.setOpt(new curlpp::options::HttpHeader(header));
    // Perform request
    request.perform();
    // Return result
    return curlpp::infos::ResponseCode::get(request);
}
