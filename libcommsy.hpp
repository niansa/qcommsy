﻿/*
    QCommsy
    Copyright (C) 2020  niansa

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/
#include <string>
#include <vector>
#include <exception>


namespace taskState {
    enum type {none, done, inProgress, todo};
}


struct commsyFile {
    std::string name;
    std::string url;
};

struct commsyPost {
    std::string name;
    std::string id;
    std::string description = "\xFF";
    std::string meta;
    std::string url;
    bool unread;
    taskState::type taskState;
    std::vector<commsyFile> files;
};


#define libCommsy_NAME "libcommsy"
#define libCommsy_VERSION "1.4-stable"
class libCommsy {
public:
    std::vector<commsyPost> posts;
    unsigned long numposts;
    std::string lastID;
    std::string server_url;
    std::string server_sid;

    bool postExists(unsigned long postID);

    commsyPost *getPost(unsigned long postID);

    std::string *getDescription(unsigned long postID);

    libCommsy(const std::string& _server_url, const std::string& _server_sid, const std::string& _room, const std::string start_id = "", const unsigned long max_posts = 0);

    // Exceptions
    class invalidSIDError {};
    class invalidRoomError {};
    class invalidPostError {};
    class connectionFailError {};
    class descDownloadError {};
    class scrapError {};
};

namespace libCommsyLowlevel {
    std::string get_post_desc(const std::string& post_url, const std::string& server_sid);
}
