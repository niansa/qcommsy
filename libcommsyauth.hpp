/*
    QCommsy
    Copyright (C) 2020  niansa

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/
#include <exception>
#include <iostream>

#include <QString>
#include <QUrl>
#include <QUrlQuery>
#include <QNetworkAccessManager>
#include <QNetworkReply>
#include <QNetworkRequest>
#include <QNetworkCookie>

#define libCommsyAuth_NAME "libcommsyauth"
#define libCommsyAuth_VERSION "0.2-stable"

class authFailureError {};


class libCommsyAuth : public QObject {
    Q_OBJECT

public:
    Q_SIGNALS:
    void connectionDone(QNetworkReply *r);
    void connectionNotfound(QNetworkReply *r);
    void authDone(QNetworkReply *r);

public:
    QNetworkAccessManager *mNetManTestconn = nullptr;
    QNetworkAccessManager *mNetManAuth = nullptr;
    QNetworkRequest *mNetReq = nullptr;

    libCommsyAuth(QObject *p = nullptr) : QObject (p){
        mNetManTestconn = new QNetworkAccessManager(this);
        mNetManAuth = new QNetworkAccessManager(this);
        mNetReq = new QNetworkRequest();
        connect(mNetManTestconn, &QNetworkAccessManager::finished, this, &libCommsyAuth::connectionDone);
        connect(mNetManAuth, &QNetworkAccessManager::finished, this, &libCommsyAuth::authDone);
    }

    ~libCommsyAuth() {
        delete mNetManTestconn;
        delete mNetManAuth;
        delete mNetReq;
    }

    void connectionInit(const QUrl& serverBaseUrl) {
        // Initialise request
        mNetReq->setUrl(serverBaseUrl);
        mNetReq->setAttribute(QNetworkRequest::Attribute::RedirectPolicyAttribute, true);
        std::cout << mNetReq->url().toString().toStdString() << std::endl;
        mNetManTestconn->get(*mNetReq);
    }

    QString getAuthUrl(const QNetworkReply *reply) {
        return reply->url().toString();
    }

    void sendAuth(const QString& authUrl, const QString& username, const QString& password) {
        QUrlQuery mNetQuery;

        // Set auth credentials
        mNetQuery.addQueryItem("user_id", username);
        mNetQuery.addQueryItem("password", password);

        // Create request
        mNetReq->setUrl(authUrl + "&mod=context&fct=login");
        mNetReq->setHeader(QNetworkRequest::ContentTypeHeader, "application/x-www-form-urlencoded");
        mNetReq->setAttribute(QNetworkRequest::RedirectPolicyAttribute, false);

        // Perform the request
        std::cout << mNetReq->url().toString().toStdString() << std::endl;
        mNetManAuth->post(*mNetReq, mNetQuery.toString(QUrl::FullyEncoded).toUtf8());
    }

    QString getSID(QNetworkReply *reply) {
        auto cookies = QNetworkCookie::parseCookies(reply->rawHeader("Set-Cookie"));

        for (const auto& cookie : cookies) {
            std::cout << '"' << cookie.name().toStdString() << "\" = \"" << cookie.value().toStdString() << '"' << std::endl;
            if (cookie.name() == "SID") {
                return cookie.value();
            }
        }

        throw authFailureError();
    }
};
